{
  description = "A very rusty flake";

  outputs = {
    self,
    nixpkgs,
  }: let
    systems = ["x86_64-linux" "aarch64-linux"];

    config = system: let
      pkgs = import nixpkgs {inherit system;};
      cargoToml = builtins.fromTOML (builtins.readFile ./Cargo.toml);
    in {
      packages.${system}.default = pkgs.rustPlatform.buildRustPackage {
        pname = cargoToml.package.name;
        version = cargoToml.package.version;
        src = ./.;

        cargoLock = {
          lockFile = ./Cargo.lock;
          outputHashes = {
            "tree-sitter-nix-0.0.1" = "sha256-hzHCcKl3T+zEHhK5U4Ym+puvOjgr+etdDHNpuCbOSzU=";
          };
        };

        nativeCheckInputs = with pkgs; [pandoc];
      };

      devShells.${system}.default = pkgs.mkShell {
        packages = with pkgs; [
          rustc
          cargo
          rustfmt
          rust-analyzer
          nil
          pandoc
        ];
      };

      formatter.${system} = pkgs.alejandra;
    };

    mergeSystems = acc: system:
      nixpkgs.lib.recursiveUpdate acc (config system);
  in
    builtins.foldl' mergeSystems {} systems;
}
