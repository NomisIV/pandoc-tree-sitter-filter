# Hello World

## Haskell

```haskell
main :: IO ()
main = putStrLn "Hello World!"
```

## Rust

```rust
fn main() {
    println!("Hello World!")
}
```

## C

```c
#include <stdio.h>
int main() {
    printf("Hello, World!");
    return 0;
}
```

## Nix

```nix
pkgs.stdenv.mkDerivation {
  name = "hello";
  buildPhase = ''
    echo "Hello World!"
    exit 1
  ''
}
```

## Html

```html
<html>
  <head>
    <title>Hello World!</title>
  </head>
  <body>
    <h1>Hello World</h1>
  </body>
</html>
```

## Css

```css
p.hello .world {
  color: red;
}
```

## JavaScript

```js
console.log("Hello World!");
```

## Json

```json
{
  "hello": "world"
}
```

## Toml

```toml
hello = "world"
```

## Dot

```dot
graph {
  hello -- world
}
```

## Markdown

```md
# Hello

*world*
```
