# `pandoc-tree-sitter-filter`

Tree-sitter is a code parser used in many text editors,
and this filter brings this very syntax engine to pandoc.
This filter works by transforming all code blocks with a language
into the same style of HTML that pandoc would generate itself,
but with the Tree-sitter node names instead.

## Usage

Install `pandoc-tree-sitter-filter` to your `$PATH`

```console
pandoc --filter pandoc-tree-sitter-filter file.md -t html
```

Make sure to add a style sheet for styling the code tags.
Check out [example-stylesheet.css](./gruvbox.css)

## Currently supported languages

(It's trivial to add more, and more will be added)

- Haskell
- Rust
- C
- Nix
- Html
- Css
- JavaScript
- Json
- Toml
- Dot


## Limitations

- Only HTML output is supported. Filters cannot know which format is being rendered in the end.

## Developing

`pandoc-tree-sitter-filter` can be tested like this:

```sh
pandoc test.md -t json | cargo run | pandoc -f json -c gruvbox.css -s -o test.html
```

or this

```sh
cargo build && pandoc test.md -F target/debug/pandoc-tree-sitter-filter -c gruvbox.css -s -o test.html
```

then open `test.html` in your favorite browser:

```sh
firefox test.html
```
