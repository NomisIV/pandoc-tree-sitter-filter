use pandoc_ast::{Block, Format, Pandoc};
use std::io::{self, Read, Write};
use tree_sitter_highlight::{
    Highlight, HighlightConfiguration, HighlightEvent, Highlighter,
};

const HIGHLIGHT_NAMES: [&str; 51] = [
    "attribute",
    "carriage-return",
    "comment",
    "comment.documentation",
    "constant",
    "constant.builtin",
    "constructor",
    "constructor.builtin",
    "embedded",
    "error",
    "escape",
    "function",
    "function.builtin",
    "keyword",
    "markup",
    "markup.bold",
    "markup.heading",
    "markup.italic",
    "markup.link",
    "markup.link.url",
    "markup.list",
    "markup.list.checked",
    "markup.list.numbered",
    "markup.list.unchecked",
    "markup.list.unnumbered",
    "markup.quote",
    "markup.raw",
    "markup.raw.block",
    "markup.raw.inline",
    "markup.strikethrough",
    "module",
    "number",
    "operator",
    "property",
    "property.builtin",
    "punctuation",
    "punctuation.bracket",
    "punctuation.delimiter",
    "punctuation.special",
    "string",
    "string.escape",
    "string.regexp",
    "string.special",
    "string.special.symbol",
    "tag",
    "type",
    "type.builtin",
    "variable",
    "variable.builtin",
    "variable.member",
    "variable.parameter",
];

#[derive(Debug)]
struct Meta {
    id: Option<String>,
    lang: Option<Language>,
    line_numbers: bool,
    start_from: u16,
}

impl Meta {
    fn from_attr(attr: &(String, Vec<String>, Vec<(String, String)>)) -> Self {
        let (lang, tags, attrs) = attr;
        Self {
            id: if lang.is_empty() {
                None
            } else {
                Some(lang.clone())
            },
            lang: tags.iter().rev().find_map(|tag| match tag.as_str() {
                "hs" | "haskell" => Some(Language::Haskell),
                "rs" | "rust" => Some(Language::Rust),
                "c" => Some(Language::C),
                "nix" => Some(Language::Nix),
                "html" => Some(Language::Html),
                "css" => Some(Language::Css),
                "js" | "javascript" => Some(Language::JavaScript),
                "json" => Some(Language::Json),
                "toml" => Some(Language::Toml),
                // "dot" | "graphviz" => Some(Language::Dot),
                "zig" => Some(Language::Zig),
                "ex" | "elixir" => Some(Language::Elixir),
                _ => None,
            }),
            line_numbers: tags.contains(&"numberLines".to_string()),
            start_from: attrs
                .iter()
                .rev()
                .find_map(|(k, v)| match k.as_str() {
                    "startFrom" => Some(v.parse().unwrap()),
                    _ => None,
                })
                .unwrap_or(1),
        }
    }
}

#[derive(Debug)]
enum Language {
    Haskell,
    Rust,
    C,
    Nix,
    Html,
    Css,
    JavaScript,
    Json,
    Toml,
    Zig,
    Elixir,
}

fn highlight_configuration(lang: &Language) -> HighlightConfiguration {
    let mut config = match lang {
        Language::Haskell => HighlightConfiguration::new(
            tree_sitter_haskell::LANGUAGE.into(),
            "haskell",
            tree_sitter_haskell::HIGHLIGHTS_QUERY,
            "",
            tree_sitter_haskell::LOCALS_QUERY,
        ),

        Language::Rust => HighlightConfiguration::new(
            tree_sitter_rust::language(),
            "rust",
            tree_sitter_rust::HIGHLIGHT_QUERY,
            "",
            "",
        ),

        Language::C => HighlightConfiguration::new(
            tree_sitter_c::LANGUAGE.into(),
            "c",
            tree_sitter_c::HIGHLIGHT_QUERY,
            "",
            "",
        ),

        Language::Nix => HighlightConfiguration::new(
            tree_sitter_nix::language(),
            "nix",
            tree_sitter_nix::HIGHLIGHTS_QUERY,
            "",
            "",
        ),

        // Language::Html => HighlightConfiguration::new(
        //     tree_sitter_html::LANGUAGE.into(),
        //     "html",
        //     tree_sitter_html::HIGHLIGHT_QUERY,
        //     tree_sitter_html::INJECTION_QUERY,
        //     "",
        // ),
        Language::Html => {
            todo!("https://github.com/tree-sitter/tree-sitter-html/pull/109")
        }

        Language::Css => HighlightConfiguration::new(
            tree_sitter_css::language(),
            "css",
            tree_sitter_css::HIGHLIGHTS_QUERY,
            "",
            "",
        ),

        Language::JavaScript => HighlightConfiguration::new(
            tree_sitter_javascript::LANGUAGE.into(),
            "javascript",
            tree_sitter_javascript::HIGHLIGHT_QUERY,
            tree_sitter_javascript::INJECTIONS_QUERY,
            tree_sitter_javascript::LOCALS_QUERY,
        ),

        Language::Json => HighlightConfiguration::new(
            tree_sitter_json::LANGUAGE.into(),
            "json",
            tree_sitter_json::HIGHLIGHTS_QUERY,
            "",
            "",
        ),

        Language::Toml => HighlightConfiguration::new(
            tree_sitter_toml_ng::language(),
            "toml",
            tree_sitter_toml_ng::HIGHLIGHTS_QUERY,
            "",
            "",
        ),

        Language::Zig => HighlightConfiguration::new(
            tree_sitter_zig::LANGUAGE.into(),
            "zig",
            tree_sitter_zig::HIGHLIGHTS_QUERY,
            tree_sitter_zig::INJECTIONS_QUERY,
            "",
        ),

        Language::Elixir => HighlightConfiguration::new(
            tree_sitter_elixir::LANGUAGE.into(),
            "elixir",
            tree_sitter_elixir::HIGHLIGHTS_QUERY,
            tree_sitter_elixir::INJECTIONS_QUERY,
            "",
        ),
    }
    .unwrap();
    config.configure(&HIGHLIGHT_NAMES);
    config
}

fn convert_code_block(meta: Meta, code: String, nr: u16) -> Block {
    let mut highlighter = Highlighter::new();

    let lang_config = highlight_configuration(&meta.lang.unwrap());

    let highlights = highlighter
        .highlight(&lang_config, code.as_bytes(), None, |_| None)
        .unwrap();

    let id = meta.id.unwrap_or(format!("ts{nr}"));

    let mut line_nr = meta.start_from - 1;

    let mut html = if meta.line_numbers {
        format!(
            r#"<div id="{id}" class="sourceCode"><pre class="sourceCode numberSource numberLines"><code class="sourceCode" style="counter-reset: source-line {line_nr}">"#,
        )
    } else {
        format!(
            r#"<div id="{id}" class="sourceCode"><pre class="sourceCode"><code class="sourceCode">"#,
        )
    };

    line_nr += 1;

    html.push_str(&format!(
        r##"<span id="{id}-{line_nr}"><a href="#{id}-{line_nr}" aria-hidden={hidden}></a>"##,
        hidden = !meta.line_numbers
    ));

    line_nr += 1;

    let mut highlight_name = None;

    for x in highlights {
        match x.unwrap() {
            HighlightEvent::HighlightStart(Highlight(n)) => {
                highlight_name = Some(HIGHLIGHT_NAMES[n]);
                html.push_str(
                    format!(
                        r#"<span class="{}">"#,
                        highlight_name.unwrap().replace(".", " ")
                    )
                    .as_str(),
                )
            }
            HighlightEvent::Source { start, end } => {
                let mut lines = code.as_str()[start..end].split('\n');

                let last_line = lines.next_back().unwrap();

                while let Some(line) = lines.next() {
                    html.push_str(html_escape::encode_text_to_string(
                        line,
                        &mut String::new(),
                    ));

                    html.push_str(&format!(
                        r##"</span></span><br><span id="{id}-{line_nr}">"##
                    ));

                    html.push_str(&format!(
                        "<a href=\"#{id}-{line_nr}\" aria-hidden={hidden}></a>",
                        hidden = !meta.line_numbers
                    ));

                    if !line.is_empty() {
                        if let Some(h) = highlight_name {
                            html.push_str(&format!(
                                r##"<span class="{highlight}">"##,
                                highlight = h.replace(".", " ")
                            ));
                        }
                    }

                    line_nr += 1;
                }

                html.push_str(html_escape::encode_text_to_string(
                    last_line,
                    &mut String::new(),
                ));
            }
            HighlightEvent::HighlightEnd => {
                html.push_str("</span>");
            }
        };
    }

    html.push_str("</span></code></pre></div>");

    Block::RawBlock(Format("html".to_string()), html)
}

fn traverse(mut pandoc: Pandoc) -> Pandoc {
    let mut code_block_counter = 0;
    pandoc.blocks = pandoc
        .blocks
        .iter()
        .map(|block| match block {
            Block::CodeBlock(attr, code)
                if Meta::from_attr(attr).lang.is_some() =>
            {
                code_block_counter += 1;
                convert_code_block(
                    Meta::from_attr(attr),
                    code.clone(),
                    code_block_counter,
                )
            }
            _ => block.clone(),
        })
        .collect();

    pandoc
}

fn main() {
    let mut s = String::new();
    io::stdin().read_to_string(&mut s).unwrap();
    let out = pandoc_ast::filter(s, traverse);
    io::stdout().write(out.as_bytes()).unwrap();
}

#[cfg(test)]
mod tests {
    use std::{
        error::Error,
        process::{Command, Stdio},
    };

    use super::*;

    fn run_command_with_stdin(
        cmd: &mut Command,
        input: &str,
    ) -> Result<String, Box<dyn Error>> {
        let mut child =
            cmd.stdin(Stdio::piped()).stdout(Stdio::piped()).spawn()?;

        let mut stdin = child.stdin.take().unwrap();

        let input2 = input.to_owned();

        std::thread::spawn(move || {
            stdin
                .write_all(input2.as_bytes())
                .expect("Could not write stdin")
        });

        let output = child.wait_with_output()?;

        assert!(output.status.success());

        Ok(String::from_utf8(output.stdout)?)
    }

    fn filter(s: &str) -> String {
        let json = run_command_with_stdin(
            &mut Command::new("pandoc").args(["-t", "json"]),
            s,
        )
        .unwrap();
        let new_json = pandoc_ast::filter(json, traverse);
        run_command_with_stdin(
            &mut Command::new("pandoc").args(["-f", "json"]),
            &new_json,
        )
        .unwrap()
    }

    #[test]
    fn it_works() {
        let result = filter(
            r#"
```rust
    fn main () {
        println!("Hello World!")
    }
```
            "#,
        );
        let expected = "<div id=\"ts1\" class=\"sourceCode\"><pre class=\"sourceCode\"><code class=\"sourceCode\"><span id=\"ts1-1\"><a href=\"#ts1-1\" aria-hidden=true></a>    <span class=\"keyword\">fn</span> <span class=\"function\">main</span> <span class=\"punctuation bracket\">(</span><span class=\"punctuation bracket\">)</span> <span class=\"punctuation bracket\">{</span></span></span><br><span id=\"ts1-2\"><a href=\"#ts1-2\" aria-hidden=true></a>        <span class=\"function\">println</span><span class=\"function\">!</span><span class=\"punctuation bracket\">(</span><span class=\"string\">\"Hello World!\"</span><span class=\"punctuation bracket\">)</span></span></span><br><span id=\"ts1-3\"><a href=\"#ts1-3\" aria-hidden=true></a>    <span class=\"punctuation bracket\">}</span></span></code></pre></div>\n";
        assert_eq!(result, expected)
    }
}
